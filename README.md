# README #

This is a repository to manage and backup all lake related scripts and databases.


# FOLDER STRUCTURE #

- release
    - docs
    - scripts
        - pig
        - python
        - shell
    - queries
    - workflow
- archive