ve = LOAD 's3a://zapr-lake/adtech_data_live/2016-09-13/' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') AS (jsonLive:map[]);

delayed = LOAD 's3a://zapr-lake/adtech_data_delayed/2016-09-13/' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') AS (jsonDelayed:map[]);

live_data = FOREACH live {    
    GENERATE 
		jsonLive#'auctionId' AS auctionid:chararray,
		jsonLive#'userId' AS userid:chararray,
		(int)jsonLive#'adId' AS adid:int,
		(map[])jsonLive#'eventTypeToEventTimestamp' AS eventTypeToEventTimestamp:map[];}

live_data_win = FILTER live_data BY eventTypeToEventTimestamp#'WIN' is not null;

live_data_win_flat = foreach live_data_win { 
	generate 
    	auctionid AS auctionid:chararray,
        userid AS userid:chararray,
        (int)adid AS adid:int,
        eventTypeToEventTimestamp#'WIN' As EventWin:chararray;
    }

delayed_data = FOREACH delayed {    
    GENERATE 
		jsonDelayed#'auctionId' AS auctionid:chararray,
		jsonDelayed#'userId' AS userid:chararray,
		(int)jsonDelayed#'adId' AS adid:int,
		(map[])jsonDelayed#'eventTypeToEventTimestamp' AS eventTypeToEventTimestamp:map[];}

delayed_data_render = FILTER delayed_data BY eventTypeToEventTimestamp#'RENDER' is not null;

delayed_data_render_flat = foreach delayed_data_render { 
	generate 
    	auctionid AS auctionid:chararray,
        userid AS userid:chararray,
        (int)adid AS adid:int,
        eventTypeToEventTimestamp#'RENDER' As EventRender:chararray;
    }

join_data = JOIN live_data_win_flat by (auctionid) LEFT OUTER, 
				 delayed_data_render_flat by (auctionid);


join_data_output = foreach join_data {
	generate 
    	  live_data_win_flat::auctionid AS auctionid:chararray,
          live_data_win_flat::userid AS userid:chararray,
          live_data_win_flat::EventWin AS EventWin:chararray,
          delayed_data_render_flat::EventRender AS EventRender:chararray;         
	}

join_data_filtered = filter join_data_output by EventRender is not null;
join_data_limit = limit join_data_filtered 100;

--describe join_data_output;

dump join_data_limit;

