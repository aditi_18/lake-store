set mapred.reduce.tasks=1;
create table zapr_lake.channel_user_list
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
stored as TEXTFILE as
select channel_name,
    collect_set(distinct user_id) as user_list 
from viewership 
where `date`='2016-08-08'
group by channel_name;
