SET hive.exec.dynamic.partition=true;
SET hive.exec.max.dynamic.partitions=2048;
SET hive.exec.max.dynamic.partitions.pernode=256;
SET hive.exec.dynamic.partition.mode=non-strict;
SET mapred.reduce.tasks=1;

INSERT INTO zapr_testcases.testcase_result PARTITION(`date`)
SELECT 'TEST0001_ADTECH' as `id`,
B.status as `status`, 
'${date}' as `date`
FROM
  (SELECT case when A.responses > 0 then 'PASSED' else 'FAILED' END as `status`
  FROM
    (SELECT count(*) as `responses` 
    FROM zapr_warehouse.ad_tech_data 
    WHERE date='${date}')A)B;

------------------------------------------------------------------------------------------------------------------------------------------------

SET hive.exec.dynamic.partition=true;
SET hive.exec.max.dynamic.partitions=2048;
SET hive.exec.max.dynamic.partitions.pernode=256;
SET hive.exec.dynamic.partition.mode=non-strict;
SET mapred.reduce.tasks=1;

INSERT INTO zapr_testcases.testcase_result PARTITION(`date`)
select 'TEST0002_ADTECH' as `id`,
B.status as `status`, 
'${date}'as `date`
FROM
    (SELECT  
    case when A.bid_responses > 0 AND 
    A.event_win>0 and A.event_render > 0 
    AND A.event_click > 0 
    then 'PASSED' else 'FAILED' END as `status`
    FROM 
        (SELECT count(`auction_id`) as `bid_responses` ,
        count(`event_win`) as `event_win`,
        count(event_render) as `event_render`,
        count(event_click) as `event_click`
        FROM zapr_warehouse.ad_tech_data 
        WHERE date='${date}')A)B;

-------------------------------------------------------------------------------------------------------------------------------------------------

SET hive.exec.dynamic.partition=true;
SET hive.exec.max.dynamic.partitions=2048;
SET hive.exec.max.dynamic.partitions.pernode=256;
SET hive.exec.dynamic.partition.mode=non-strict;
SET mapred.reduce.tasks=1;

INSERT INTO zapr_testcases.testcase_result PARTITON(`date`)
SELECT 'TEST0003_ADTECH' as `id`,
B.status as `status`, 
'${date}' as `date`
FROM
    (SELECT
    case when A.bid_responses > A.event_win 
    AND A.bid_responses > A.event_render 
    AND A.bid_responses > A.event_click 
    then 'PASSED' else 'FAILED' END as `status`
    FROM 
        (SELECT count(`auction_id`) as `bid_responses` ,
        count(`event_win`) as `event_win`,
        count(event_render) as `event_render`,
        count(event_click) as `event_click`
        FROM zapr_warehouse.ad_tech_data 
        WHERE date='${date}')A)B;

------------------------------------------------------------------------------------------------------------------------------------------------

SET hive.exec.dynamic.partition=true;
SET hive.exec.max.dynamic.partitions=2048;
SET hive.exec.max.dynamic.partitions.pernode=256;
SET hive.exec.dynamic.partition.mode=non-strict;
SET mapred.reduce.tasks=1;

INSERT INTO zapr_testcases.testcase_result PARTITION(`date`)
SELECT 'TEST0004_ADTECH' as `id`,
B.status as `status`,
'${date}' as `date`
FROM
    (SELECT
    case when A.auction_less_events > 0
    then 'FAILED' else 'PASSED' END as `status`
    FROM
        (SELECT 
        count(*) as `auction_less_events`
        FROM zapr_warehouse.ad_tech_data 
        WHERE date='${date}'
        AND auction_id is null)A)B;

------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO zapr_testcases.testcase_result PARTITION(`date`)
SELECT 'TEST0005_ADTECH' as `id`,E.status as `status` , '${current_date}' as `date`
FROM
(SELECT
case when D.current_responses > (D.responses_yesterday - 0.5*D.responses_yesterday) 
AND D.current_responses < (D.responses_yesterday + 0.5*D.responses_yesterday)
AND D.current_responses > (D.responses_day_before - 0.5*D.responses_day_before) 
AND D.current_responses < (D.responses_day_before + 0.5*D.responses_day_before)
then 'PASSED' else 'FAILED' END as `status`
FROM
    (SELECT 
    A.current_responses,B.responses_yesterday,C.responses_day_before 
    FROM 
        (SELECT 
        count(auction_id) as current_responses,'TEST0005_ADTECH' as id 
        FROM zapr_warehouse.ad_tech_data 
        WHERE date='${current_date}')A
    JOIN
        (SELECT 
        count(auction_id) as `responses_yesterday`, 
        'TEST0005_ADTECH' as id 
        FROM zapr_warehouse.ad_tech_data 
        WHERE date='${yesterday_date}')B
    ON A.id=B.id
    JOIN
        (SELECT 
        count(auction_id) as `responses_day_before`,
        'TEST0005_ADTECH' as id 
        FROM zapr_warehouse.ad_tech_data 
        WHERE date='${day_before_date}')C
    ON C.id=B.id)D)E;

